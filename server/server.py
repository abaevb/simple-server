from flask import Flask, escape, request
from jinja2 import Environment, FileSystemLoader
from prometheus_client import Counter, make_wsgi_app, Summary
import random 
from werkzeug.middleware.dispatcher import DispatcherMiddleware
import time
import requests

app = Flask(__name__)
HELLOS = Counter('total_hellos', "Number of total hellos requested")
LATENCY = Summary('hello_latency_seconds', "Latency of request")

# uwsgi --http 0.0.0.0:5000 --wsgi-file app.py --callable app_dispatch
app_dispatch = DispatcherMiddleware(app, {
    '/metrics': make_wsgi_app()
})

@app.route('/')
def hello():
    HELLOS.inc()
    start = time.time()
    try:
        friend_said = requests.get('http://friend.default.svc.cluster.local:80').content
    except:
        friend_said = b"Something is off..."
    env = Environment(loader=FileSystemLoader("templates"))
    template = env.get_template("index.html")
    page = template.render(data={"friend":"My friend said: {}".format(friend_said.decode("utf-8") )})
    LATENCY.observe(time.time() - start)
    return page


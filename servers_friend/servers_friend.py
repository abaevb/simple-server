from flask import Flask, escape, request
from prometheus_client import Counter, make_wsgi_app, Summary
import random 
from werkzeug.middleware.dispatcher import DispatcherMiddleware
import time

app = Flask(__name__)
WAVES = Counter('total_waves', "Number of total waves requested")
LATENCY = Summary('waves_latency_seconds', "Latency of request")

# uwsgi --http 0.0.0.0:5000 --wsgi-file app.py --callable app_dispatch
app_dispatch = DispatcherMiddleware(app, {
    '/metrics': make_wsgi_app()
})


@app.route('/')
def wave():
    start = time.time()
    words = ['Hi', 'Hello', 'What up', 'Greetings', 'Kuku' 'BubU!']
    word = words[random.randint(0,len(words)-1)]
    WAVES.inc()
    LATENCY.observe(time.time() - start)
    return word

